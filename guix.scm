(use-modules (guix gexp)
             (guix packages)
             (guix git-download)
             (guix utils)
             (guix build-system gnu)
             (gnu packages autotools)
             (gnu packages guile)
             (gnu packages pkg-config)
             (gnu packages sdl)
             (gnu packages texinfo)
             ((guix licenses) #:select (lgpl3+)))

(define %source-dir (dirname (current-filename)))

(package
  (name "tsukundere")
  (version "0.2.0")
  (source (local-file %source-dir
                      #:recursive? #t
                      #:select? (git-predicate %source-dir)))
  (build-system gnu-build-system)
  (native-inputs
   `(("autoconf" ,autoconf-wrapper)
     ("automake" ,automake)
     ("guile" ,guile-3.0)
     ("pkg-config" ,pkg-config)
     ("texinfo" ,texinfo)))
  (propagated-inputs
   `(("guile-sdl2" ,guile3.0-sdl2)))
  (home-page "https://gitlab.com/leoprikler/tsukundere")
  (synopsis "Visual novel engine")
  (description "Tsukundere is a game engine geared heavily towards the
development of visual novels, written on top of Guile-SDL2.  It is still
experimental and at the time of writing contains little more than the Guile
modules, that make up its runtime.")
  (license lgpl3+))
