;; Copyright (C) 2020, Leo Prikler <leo.prikler@student.tugraz.at>
;;
;; This library is free software; you can redistribute it and/or
;; modify it under the terms of the GNU Lesser General Public
;; License as published by the Free Software Foundation; either
;; version 3 of the License, or (at your option) any later version.
;;
;; This library is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; Lesser General Public License for more details.
;;
;; You should have received a copy of the GNU Lesser General Public
;; License along with this library; if not, write to the Free Software
;; Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA

(define-module (tsukundere game)
  #:use-module (sdl2)
  #:use-module (sdl2 events)
  #:use-module (sdl2 render)
  #:use-module (sdl2 video)
  #:use-module (sdl2 mixer)
  #:use-module (sdl2 ttf)
  #:use-module (system foreign)
  #:use-module (tsukundere agenda)
  #:use-module (tsukundere game internals)
  #:use-module (tsukundere history)
  #:use-module (tsukundere preferences)
  #:use-module (tsukundere scene)
  #:use-module (tsukundere script)
  #:use-module (tsukundere text)
  #:use-module (tsukundere utils)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-2)
  #:use-module (srfi srfi-26)
  #:use-module (ice-9 format)
  #:use-module (ice-9 match)
  #:use-module (ice-9 optargs)

  #:re-export (current-window
               current-renderer)

  #:export (run-game
            call-with-game-loop
            call-with-game-loop/paused
            init-script! init-text!
            step! skip!))

(define sdl-func
  ;; TODO: Upstream everything, that relies on this to Guile-SDL2
  (@@ (sdl2 bindings) sdl-func))

(define sdl-get-window-minimum-size
  (sdl-func void "SDL_GetWindowMinimumSize" (list '* '* '*)))
(define sdl-set-window-minimum-size
  (sdl-func void "SDL_SetWindowMinimumSize" (list '* int int)))
(define sdl-set-window-resizable
  (sdl-func void "SDL_SetWindowResizable" (list '* int)))
(define sdl-render-set-viewport
  (sdl-func int "SDL_RenderSetViewport" (list '* '*)))
(define sdl-render-set-scale
  (sdl-func int "SDL_RenderSetScale" (list '* float float)))

(define (window-minimum-size window)
  ((@@ (sdl2 video) %get-coords)
   window
   sdl-get-window-minimum-size))

(define (set-window-minimum-size! window size)
  (match size
    ((width height)
     (sdl-set-window-minimum-size
      ((@@ (sdl2 video) unwrap-window) window)
      width height))))

(define (set-window-resizable! window resizable?)
  (sdl-set-window-resizable
                  ((@@ (sdl2 video) unwrap-window) window)
                  ((@@ (sdl2 bindings) boolean->sdl-bool) resizable?)))

(define (render-set-scale renderer scale)
  ;; For our purposes, we only need a single scale factor
  (sdl-render-set-scale ((@@ (sdl2 render) unwrap-renderer) renderer)
                        scale scale))
(define (render-set-viewport renderer rect)
  (sdl-render-set-viewport ((@@ (sdl2 render) unwrap-renderer) renderer)
                           ((@@ (sdl2 rect) unwrap-rect) rect)))

;; Real Tsukundere code starts here

(define *fullscreen?* (make-preference 'fullscreen? #f #:validate? boolean?))
(add-preference-hook!
 *fullscreen?*
 (lambda (value)
   (and-let* ((window (current-window)))
     (set-window-fullscreen! window value #:desktop? value))))

(define (fit-current-window!)
  (let* ((window (current-window))
         (requested (window-minimum-size window))
         (given (window-size window)))
    (if (any < given requested)
        (begin
          (set-window-resizable! window #f)
          (set-window-size! window (map min requested given))
          (fit-current-window!)
          (set-window-resizable! window #t))
        (let* ((scale (apply min (map / given requested)))
               (used (map (compose floor (cute * <> scale)) requested))
               (offset (map (compose (cute floor/ <> 2) -) given used)))
          (render-set-viewport (current-renderer)
                               (apply
                                (@@ (sdl2 rect) make-rect)
                                (append offset used)))
          (render-set-scale (current-renderer) scale)))))

(define *initial-state* (make-unbound-fluid))
(define *skipping* (make-fluid #f))

(define (is-running?)
  "Return #t if a script is currently running, otherwise #f."
  (and (fluid-bound? *script*)
       (eq? (script-status (current-script)) 'running)))

;; Set up the main script of the game to execute @var{thunk}, also setting the
;; scene to @var{scene}.  While this function does not barf if called multiple
;; times, it really should only be called once.
(define* (init-script! thunk #:optional (scene (make-scene '())))
  "Initialize the script library, setting THUNK as script and SCENE as scene.
THUNK is not executed until start! is called."
  (fluid-set! *scene* scene)
  (fluid-set! *script*
              (make-script
               (lambda ()
                 (thunk)
                 (abort-to-prompt quit-prompt))))
  (fluid-set! *initial-state* (current-dynamic-state)))

;; Initialize the @code{text} layer of @var{scene}, adding a text object
;; for dialogue at @code{(s32vector x text-y)} and for the speaker at
;; @code{(s32vector x speaker-y)}.
(define* (init-text! x text-y speaker-y
                     text-font speaker-font
                     #:key (scene (fluid-ref *scene*)))
  "Initialize speaker and text objects at (X SPEAKER_Y) and (X TEXT_Y)
in the 'text layer using SPEAKER_FONT and TEXT_FONT respectively."
  (scene-clear! scene 'text)
  (scene-add-object! scene 'text
                     (make-text (s32vector x speaker-y) ""
                                speaker-font))
  (scene-add-object! scene 'text
                     (make-text (s32vector x text-y) ""
                                text-font #:length 0)))

(define (update delta)
  "Update the script clock and advance the text."
  (when (is-running?)
    (if (fluid-ref *skipping*)
        (skip! 1)
        (update-agenda! (script-clock (current-script)) delta))
    (match (scene-layer (fluid-ref *scene*) 'text)
      (#f *unspecified*)
      ((speaker text)
       (text-advance! text delta)))))

(define (draw)
  "Draw the current scene."
  (draw-scene (fluid-ref *scene*)))

(define* (start! #:key (lines 0) (status 'running))
  "Start the script, optionally skipping LINES, a number of lines."
  (and-let* ((script (current-script)))
    (set-script-status! script 'cancelled))
  (reset-fluids! (list *script* *scene*) (fluid-ref *initial-state*))
  (let ((script (fresh-script (fluid-ref *script*))))
    (fluid-set! *script* script)
    (start-script script #:status status)
    (skip! lines)
    (set-script-status! script 'running)))

(add-load-hook!
 (lambda* (#:key tsukundere-line #:allow-other-keys)
   (start! #:lines tsukundere-line #:status 'loading)))

(define quit-prompt (make-prompt-tag))

;; Return a thunk, that if called, polls SDL for events and runs the
;; corresponding handler.
;; @*
;; @var{key-press} is called with four arguments when a key is pressed.
;; @itemize
;; @item @var{key}: The key, that was pressed.
;; @item @var{scancode}: The raw scancode of the key, that was pressed.
;; This can be different from @var{key}, but mostly shouldn't.
;; @item @var{modifiers}: Which modifier keys (e.g. shift, control, alt)
;; where also pressed at the same time.
;; @item @var{repeat?}: Whether this key was already pressed down.
;; @end itemize
;; @var{key-release} is likewise called when a key is released with the
;; arguments @var{key}, @var{scancode} and @var{modifiers} as above.
;; @*
;; @var{mouse-press} is called with three arguments when a mouse key is pressed.
;; @itemize
;; @item @var{button}: Which button was pressed.
;; @item @var{clicks}: How often @var{button} has been clicked.
;; @item @var{pos}: The position at which @var{button} was clicked as
;; @code{(s32vector x y)}.
;; @end itemize
;; @var{mouse-release} is likewise called when a mouse button is released
;; with the arguments @var{button}, @var{pos}.
;; @*
;; @var{mouse-move} is called with three arguments when the mouse pointer is
;; moved.
;; @itemize
;; @item @var{buttons}: Which mouse buttons are currently pressed.
;; @item @var{pos}: The position to which the mouse was moved.
;; @item @var{rel}: The relative movement of the mouse.
;; @end itemize
;; @var{text-input} is called with a singular argument @var{text}, whenever
;; a printable key is pressed, where text holds that key(s) as string.
(define* (event-handler #:key
                        key-press
                        key-release
                        mouse-press
                        mouse-release
                        mouse-move
                        text-input)
  (lambda ()
    (let loop ((event (poll-event)))
      (when event
       (cond
        ((quit-event? event) (abort-to-prompt quit-prompt))
        ((keyboard-down-event? event)
         (key-press (keyboard-event-key event)
                    (keyboard-event-scancode event)
                    (keyboard-event-modifiers event)
                    (keyboard-event-repeat? event)))
        ((keyboard-up-event? event)
         (key-release (keyboard-event-key event)
                      (keyboard-event-scancode event)
                      (keyboard-event-modifiers event)))
        ((mouse-button-down-event? event)
         (mouse-press (mouse-button-event-button event)
                      (mouse-button-event-clicks event)
                      (s32vector
                       (mouse-button-event-x event)
                       (mouse-button-event-y event))))
        ((mouse-button-up-event? event)
         (mouse-release (mouse-button-event-button event)
                        (s32vector
                         (mouse-button-event-x event)
                         (mouse-button-event-y event))))
        ((mouse-motion-event? event)
         (mouse-move (mouse-motion-event-buttons event)
                     (s32vector
                      (mouse-motion-event-x event)
                      (mouse-motion-event-y event))
                     (s32vector
                      (mouse-motion-event-x-rel event)
                      (mouse-motion-event-y-rel event))))
        ((text-input-event? event)
         (text-input (text-input-event-text event)))
        ((window-resized-event? event)
         (fit-current-window!)))
       (loop (poll-event))))))

;; Step forward through the current script.
;; If the scene contains text, first ensure, that it is fully readable.
(define (step!)
  (match (scene-layer (fluid-ref *scene*) 'text)
    (#f (forward-line! (current-script)))
    ((_ text)
     (let ((l (text-length text))
           (s (text-string text)))
       (if (= l (string-length s))
           (forward-line! (current-script))
           (unless (speed-through-clock! (current-script))
             (set-text-length! text (string-length s))))))))

;; Skip @var{n} lines of the current script.
(define (skip! n)
  (if (> n 0)
      (begin
        (speed-through-clock! (current-script))
        (forward-line! (current-script))
        (skip! (- n 1)))
      (when (fluid-ref *skipping*)
        (match (scene-layer (fluid-ref *scene*) 'text)
          (#f *unspecified*)
          ((_ text)
           (let ((s (text-string text)))
             (set-text-length! text (string-length s))))))))

(define (key-press key code mod repeat?)
  (define non-null? (negate null?))
  (match (list key repeat?)
    ((_ #t) *unspecified*)
    (('return _) (step!))
    (('space _) (step!))
    (('left-control _) (fluid-set! *skipping* #t))
    (('right-control _) (fluid-set! *skipping* #t))
    (((? integer? n) _)
     (if (non-null? (lset-intersection eq? mod '(left-shift right-shift)))
         (save (current-game) n)
         (load (current-game) n)))
    (('f11 _)
     (set! (*fullscreen?*) (not (*fullscreen?*))))
    ((_ _) *unspecified*)))

(define (key-release key code mod)
  (case key
    ((left-control right-control)
     (fluid-set! *skipping* #f))
    (else *unspecified*)))

(define (mouse-press button clicks pos)
  (match (list button)
    (('left) (step!))
    ((_) *unspecified*)))

(define mouse-release (const *unspecified*))
(define mouse-move (const *unspecified*))
(define text-input (const *unspecified*))

;; Call @var{thunk} as a new script.
;; @var{key-press}, @var{key-release}, @var{mouse-press}, @var{mouse-release},
;; @var{mouse-move} and @var{input} are passed on to @code{event-handler} to
;; form an event handler, that is used while @var{thunk} still runs.
;; Likewise, @var{scene} will be rendered while @var{thunk} is active.
(define* (call-with-game-loop thunk
                              #:key
                              (key-press key-press)
                              (key-release key-release)
                              (mouse-press mouse-press)
                              (mouse-release mouse-release)
                              (mouse-move mouse-move)
                              (text-input text-input)

                              (scene (fluid-ref *scene*)))
  (fluid-set! *skipping* #f)
  (let ((orig-script (current-script))
        (state (current-dynamic-state)))
    (fluid-set! *event-handler* (event-handler #:key-press key-press
                                               #:key-release key-release
                                               #:mouse-press mouse-press
                                               #:mouse-release mouse-release
                                               #:mouse-move mouse-move
                                               #:text-input text-input))
    (fluid-set! *scene* scene)
    (let ((script
            (make-script
             (if orig-script
                 (lambda ()
                   (thunk)
                   (pause
                    (lambda (script)
                      (set-script-status! script 'cancelled)
                      (reset-fluids! (list *event-handler* *script* *scene*)
                                     state)
                      (resume orig-script))))
                 thunk))))
      (fluid-set! *script* script)
      (start-script script)
      (set-script-status! script 'running))))

;; @deffnx {(tsukundere script utils) alias} call
;; Call @var{thunk} as a new script while pausing the current one, while
;; establishing the same bindings, that @code{call-with-game-loop} would.
;; If @var{ignore-during-load?} is @code{#t} (the default), simply resume
;; the original script instead, if the game is currently loading.
(define* (call-with-game-loop/paused thunk
                                     #:key
                                     (scene (fluid-ref *scene*))
                                     (key-press key-press)
                                     (key-release key-release)
                                     (mouse-press mouse-press)
                                     (mouse-release mouse-release)
                                     (mouse-move mouse-move)
                                     (text-input text-input)
                                     (ignore-during-load? #t))
  (pause
   (lambda (orig-script)
     (if (and ignore-during-load?
              (eq? (script-status orig-script) 'loading))
         (resume orig-script)
         (call-with-game-loop thunk
                              #:scene scene
                              #:key-press key-press
                              #:key-release key-release
                              #:mouse-press mouse-press
                              #:mouse-release mouse-release
                              #:mouse-move mouse-move
                              #:text-input text-input)))))

(define (init-noop)
  (init-script! (lambda () (pause))))

;; Run the game @var{game-name}, which is initialized via @var{init!}.
;; @*
;; Use @var{time} to keep track of the current time, updating the game
;; state and rendering every @code{(/ 1 update-freq)} seconds.
;; @*
;; Use @var{window-title} as the title of the window @var{game-name} runs
;; in and initalize its size using @var{window-width} and @var{window-height}.
;; Optionally make the window fullscreen if @var{fullscreen?} is @code{#t}.
;; @*
;; If @var{use-mixer?} is @code{#t}, also initialize SDL mixer for sound
;; support, using @var{mixer-formats} for the supported formats in
;; @code{mixer-init},  and @var{mixer-frequency} and @var{mixer-chunk-size}
;; to set the @code{#:frequency} and @code{chunk-size} arguments in
;; @code{open-audio} respectively, also setting the sound volume to
;; @var{sound-volume} and the default music volume to @var{music-volume}.
;; @*
;; When the player tries to save, call @var{save} to produce a list of
;; @code{#:variable value} assignments, that will be saved along with all
;; the internal variables.
(define* (run-game #:key
                   (game-name (basename (car (program-arguments))))
                   (init! init-noop)

                   (time sdl-ticks)
                   (update-freq 60)

                   (window-title "Tsukundere")
                   (window-width 600)
                   (window-height 480)

                   (use-mixer? #t)
                   (mixer-formats '(flac mp3 ogg))
                   (mixer-frequency 44100)
                   (mixer-chunk-size 1024)
                   (sound-volume 64)
                   (music-volume 64)

                   (save (const '())))

  (load-preferences)
  (save-preferences #f 'unless-exists?)
  ;; Let's be extra careful to avoid path traversal
  (fluid-set! *game-name* (and=> game-name basename))
  (fluid-set! *save-state* save)
  (sdl-init)
  (ttf-init)
  (when use-mixer?
    (mixer-init mixer-formats)
    (open-audio #:frequency mixer-frequency #:chunk-size mixer-chunk-size)
    (set-channel-volume! #f sound-volume)
    (set-music-volume! music-volume))
  (fluid-set! *event-handler* (event-handler #:key-press key-press
                                             #:key-release key-release
                                             #:mouse-press mouse-press
                                             #:mouse-release mouse-release
                                             #:mouse-move mouse-move
                                             #:text-input text-input))
  (call-with-window (make-window #:title window-title
                                 #:size (list window-width window-height)
                                 #:resizable? #t
                                 #:fullscreen-desktop? (*fullscreen?*)
                                 #:show? #f)
    (lambda (window)
      (set-window-minimum-size! window (list window-width window-height))
      (fluid-set! *window* window)
      (call-with-renderer (make-renderer window)
        (lambda (renderer)
          (show-window! window)
          (fluid-set! *renderer* renderer)
          (fit-current-window!)
          (init!)
          (load-preferences game-name)
          (start!)
          (call-with-prompt quit-prompt
            (lambda ()
              (let loop ((delta (/ 1000 update-freq))
                         (last (time)))
                (let ((now (time)))
                  ((fluid-ref *event-handler*))
                  (update (- now last))
                  (clear-renderer (current-renderer))
                  (draw)
                  (present-renderer (current-renderer))
                  (usleep (floor delta))
                  (loop delta now))))
            (lambda _
              (when (current-game)
                (save-preferences (current-game))))))))))
