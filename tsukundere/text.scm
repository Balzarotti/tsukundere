;; Copyright (C) 2020, Leo Prikler <leo.prikler@student.tugraz.at>
;;
;; This library is free software; you can redistribute it and/or
;; modify it under the terms of the GNU Lesser General Public
;; License as published by the Free Software Foundation; either
;; version 3 of the License, or (at your option) any later version.
;;
;; This library is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; Lesser General Public License for more details.
;;
;; You should have received a copy of the GNU Lesser General Public
;; License along with this library; if not, write to the Free Software
;; Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA

(define-module (tsukundere text)
  #:use-module (sdl2)
  #:use-module (sdl2 surface)
  #:use-module (sdl2 ttf)
  #:use-module (ice-9 optargs)
  #:use-module (tsukundere preferences)
  #:use-module (tsukundere scene)
  #:use-module (tsukundere textures)
  #:export (make-text
            text-string set-text-string!
            text-font set-text-font!
            text-color set-text-color!
            text-length set-text-length!
            text-alignment set-text-alignment!
            reset-text! text-add!
            text-advance!
            draw-text))

(define *text-speed* (make-preference 'text-speed 24
                                      #:validate?
                                      (lambda (value)
                                        (and (number? value) (> value 0)))))
(define *font-cache* (make-hash-table))

(define <text>
  (make-record-type '<text>
                    '(string length alignment font color)
                    #:parent <object>))
(define %make-text          (record-constructor <text>))
(define text-string         (record-accessor <text> 'string))
(define text-font           (record-accessor <text> 'font))
(define text-color          (record-accessor <text> 'color))
(define text-length         (record-accessor <text> 'length))
(define text-alignment      (record-accessor <text> 'alignment))
(define set-text-string!    (record-modifier <text> 'string))
(define set-text-font!      (record-modifier <text> 'font))
(define set-text-color!     (record-modifier <text> 'color))
(define set-text-length!    (record-modifier <text> 'length))
(define set-text-alignment! (record-modifier <text> 'alignment))

;; Make a new text object @var{pos} containing @var{string} rendered with
;; @var{font} in @var{color}.  Align the text horizontically and vertically
;; according to @var{alignment} and render only @var{length} characters of it.
(define* (make-text pos string font
                    #:key
                    (color (make-color 255 255 255 255))
                    (length (string-length string))
                    (alignment '(left . top)))
  (%make-text
   pos draw-text
   string length alignment font color))

;; Reset @var{text} to contain @var{string}.  If @var{full?} is @code{#t}, also
;; set its length to that of @var{string}, so that the full string is rendered.
(define* (reset-text! text string #:optional full?)
  (set-text-length! text 0)
  (set-text-string! text string)
  (if full?
      (set-text-length! text (string-length string))))

;; Add @var{string} to the end of @var{text}.
(define (text-add! text string)
  (set-text-string! text (string-append (text-string text) string)))

(define (font-cache font color)
  (or (hash-ref *font-cache* (cons font color))
      (hash-set! *font-cache* (cons font color) (make-hash-table))))

(define (font-cache-ref font color char)
  (or (hashq-ref (font-cache font color) char)
      (hashq-set! (font-cache font color) char
                  (surface->texture*
                   (render-font-blended font (string char) color)))))

(define render-char (render-texture 'top-left))

(define (%text-origin text n-lines pos)
  (s32vector
   (s32vector-ref pos 0)
   (case (cdr (text-alignment text))
     ((top) (s32vector-ref pos 1))
     ((center)
      (- (s32vector-ref pos 1)
         (floor/ (* (font-height (text-font text)) n-lines) 2)))
     ((bottom)
      (- (s32vector-ref pos 1)
         (* (font-height (text-font text)) n-lines)))
     (else (error "Invalid text alignment")))))

(define (%line-width line font color)
  (string-fold
   (lambda (c acc)
     (+ acc (texture-width (font-cache-ref font color c))))
   0
   line))

(define (%line-origin line pos font color halign)
  (s32vector
   (case halign
     ((left) (s32vector-ref pos 0))
     ((center) (- (s32vector-ref pos 0)
                  (floor/ (%line-width line font color) 2)))
     ((right) (- (s32vector-ref pos 0)
                 (%line-width line font color))))
   (s32vector-ref pos 1)))

(define (draw-line line pos font color)
  (string-for-each
   (lambda (char)
     (let ((rendered-char (font-cache-ref font color char)))
       (render-char rendered-char pos)
       (s32vector-set! pos 0 (+ (s32vector-ref pos 0)
                                (texture-width rendered-char)))))
   line))

;; Render @var{text}, a @var{<text>} record.
(define (draw-text text)
  (let ((pos (object-pos text))
        (font (text-font text))
        (color (text-color text))
        (lines (string-split
                (substring (text-string text) 0 (floor (text-length text)))
                #\newline))
        (halign (car (text-alignment text))))
    (let ((pos (%text-origin text (length lines) pos)))
      (for-each
       (lambda (line)
         (draw-line line (%line-origin line pos font color halign)
                    font color)
         (s32vector-set! pos 1 (+ (s32vector-ref pos 1)
                                  (font-height font))))
       lines))))

;; Update the length of @var{text} according to the current text speed,
;; given that @var{delta} milliseconds have passed.
(define (text-advance! text delta)
  (let ((s (text-string text))
        (l (text-length text))
        (a (* (/ delta 1000) (*text-speed*))))
    (set-text-length! text (min (string-length s) (+ l a)))))
