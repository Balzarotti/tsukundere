;; Copyright (C) 2020, Leo Prikler <leo.prikler@student.tugraz.at>
;;
;; This library is free software; you can redistribute it and/or
;; modify it under the terms of the GNU Lesser General Public
;; License as published by the Free Software Foundation; either
;; version 3 of the License, or (at your option) any later version.
;;
;; This library is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; Lesser General Public License for more details.
;;
;; You should have received a copy of the GNU Lesser General Public
;; License along with this library; if not, write to the Free Software
;; Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA

(define-module (tsukundere game internals)
  #:use-module (tsukundere script)
  #:export (*scene*
            *script*
            *window*
            *renderer*
            *game-name*
            *event-handler*
            *save-state*

            current-window current-renderer current-game current-script))

(define *scene* (make-unbound-fluid))
(define *script* (make-unbound-fluid))
(define *window* (make-unbound-fluid))
(define *renderer* (make-unbound-fluid))
(define *game-name* (make-unbound-fluid))

(define *event-handler* (make-unbound-fluid))
(define *save-state* (make-unbound-fluid))

(define (current-window) (fluid-ref *window*))
(define (current-renderer) (fluid-ref *renderer*))
(define (current-game) (fluid-ref *game-name*))
(define (current-script) (and (fluid-bound? *script*) (fluid-ref *script*)))
