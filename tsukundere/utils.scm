;; Copyright (C) 2020, Leo Prikler <leo.prikler@student.tugraz.at>
;;
;; This library is free software; you can redistribute it and/or
;; modify it under the terms of the GNU Lesser General Public
;; License as published by the Free Software Foundation; either
;; version 3 of the License, or (at your option) any later version.
;;
;; This library is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; Lesser General Public License for more details.
;;
;; You should have received a copy of the GNU Lesser General Public
;; License along with this library; if not, write to the Free Software
;; Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA

(define-module (tsukundere utils)
  #:use-module (ice-9 match)
  #:use-module (ice-9 peg)
  #:use-module (ice-9 ftw)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-2)
  #:export (xdg-data-dir
            tsukundere-config-dir
            tsukundere-config-file
            tsukundere-data-dir
            tsukundere-data-file
            mkdir-p
            load-assets
            reset-fluids!))

(define (xdg-config-dir)
  "Return the canonical value of XDG_CONFIG_HOME."
  (string-append
   (or (getenv "XDG_CONFIG_HOME")
       (string-append (getenv "HOME") "/.config"))))

(define (xdg-data-dir)
  "Return the canonical value of XDG_DATA_HOME."
  (string-append
   (or (getenv "XDG_DATA_HOME")
       (string-append (getenv "HOME") "/.local/share"))))

(define (tsukundere-config-dir)
  "Return the directory, in which tsukundere can store configuration."
  (string-append (xdg-config-dir) "/tsukundere"))

(define (tsukundere-config-file f)
  (string-append (tsukundere-config-dir) "/" f))

(define (tsukundere-data-dir)
  "Return the directory, in which tsukundere can store data."
  (string-append (xdg-data-dir) "/tsukundere"))

(define (tsukundere-data-file f)
  (string-append (tsukundere-data-dir) "/" f))

(define (string-empty? s)
  (= (string-length s) 0))

(define (mkdir-p dir)
  "Make DIR including its parents."
  (define (mkdir? path)
    (catch 'system-error
      (lambda ()
        (mkdir path)
        #t)
      (lambda args
        (if (= EEXIST (system-error-errno args))
            #t
            (apply throw args)))))

  (let loop ((components (string-split dir #\/))
             (root (if (string-prefix? "/" dir) "" ".")))
    (cond
     ((null? components) #t)
     ((string-empty? (car components))
      (loop (cdr components) root))
     (else
      (let ((path (string-append root "/" (car components))))
       (and (mkdir? path)
            (loop (cdr components) path)))))))

;; Load assets from files in @var{directory}, whose names conform to the PEG
;; pattern @var{pattern} and convert them to something usable with
;; @var{converter}, finally storing them in an association list, whose
;; keys are formed according to @var{keys}.
;; @example
;; (use-modules (ice-9 peg) (tsukundere utils))
;; (define-peg-pattern id all (+ (or (range #\a #\z) (range #\0 #\9))))
;; (define-peg-pattern id+ all (+ (and id (? (or "/" "-")))))
;; (define-peg-pattern scm all (and id+ ".scm"))
;; (load-assets identity "/path/to/tsukundere" scm '(id))
;; @result{} ((scripts/doc/snarf . "/path/to/tsukundere/scripts/doc-snarf.scm")
;; (tsukundere/utils . "/path/to/tsukundere/tsukundere/utils.scm")
;; ...)
;; @end example
;; Note, that @code{search-for-pattern} is used to match @var{pattern} rather
;; @code{match-pattern}.  This is so that leading directories do not show up
;; in the match, but it also means, that load-assets might return more than
;; you're looking for.
(define (load-assets converter directory pattern keys)
  "Load assets conforming to PEG pattern PATTERN from DIRECTORY, converting
them to something usable using CONVERTER and associating them with KEYS."
  (file-system-fold
   (const #t)
   (lambda (file stat result)
     (or (and-let* ((%match (search-for-pattern pattern file))
                    (asset (converter file))
                    (tree (peg:tree %match))
                    (ctx (keyword-flatten keys tree))
                    (path (string-join
                           (filter-map
                            (lambda (x)
                              (and (pair? x)
                                   (member (first x) keys)
                                   (second x)))
                            ctx)
                           "/")))
           (acons (string->symbol path) asset result))
         result))
   (lambda (file stat result) result)
   (lambda (file stat result) result)
   (lambda (file stat result) result)
   (lambda (file stat errno result) result)
   '()
   directory))

(define (reset-fluids! fluids state)
  "Reset FLUIDS, a list of fluids, to the values they had at STATE."
  (for-each
   fluid-set!
   fluids
   (with-dynamic-state
    state
    (lambda ()
      (map fluid-ref fluids)))))
