;; Copyright (C) 2020, Leo Prikler <leo.prikler@student.tugraz.at>
;;
;; This library is free software; you can redistribute it and/or
;; modify it under the terms of the GNU Lesser General Public
;; License as published by the Free Software Foundation; either
;; version 3 of the License, or (at your option) any later version.
;;
;; This library is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; Lesser General Public License for more details.
;;
;; You should have received a copy of the GNU Lesser General Public
;; License along with this library; if not, write to the Free Software
;; Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA

(define-module (tsukundere scene)
  #:use-module (ice-9 ftw)
  #:use-module (srfi srfi-1)
  #:use-module (tsukundere textures)
  #:use-module ((tsukundere game internals)
                #:select (current-renderer))
  #:export (<object>
            make-object
            object-pos set-object-pos!
            render-object
            surface->object
            texture->object

            make-scene
            scene-layers
            scene-layer
            scene-add!
            scene-add-object!
            scene-add-objects!
            scene-clear!
            scene-remove-object!

            draw-scene))

(define <object>
  (make-record-type '<object> '(pos (immutable render))
                    #:extensible? #t))
(define %make-object (record-constructor <object>))
(define object-pos (record-accessor <object> 'pos))
(define set-object-pos! (record-modifier <object> 'pos))
(define object-render (record-accessor <object> 'render))

;; Make a new object at @var{pos} rendering @var{texture} using @var{render}.
;; @var{render} is called as @code{(render texture pos)}.
(define (make-object texture pos render)
  (%make-object pos (lambda (obj)
                      (render texture (object-pos obj)))))

;; Render @var{object}.
(define (render-object object)
  ((object-render object) object))

;; Convert @var{surface} to an object rendered at @var{pos} with @var{anchor}.
(define* (surface->object surface #:key
                          (pos #s32(0 0))
                          (anchor 'center))
  (texture->object (surface->texture* (current-renderer) surface)
                   #:pos pos #:anchor anchor))

;; Convert @var{texture} to an object rendered at @var{pos} with @var{anchor}.
(define* (texture->object texture #:key
                          (pos #s32(0 0))
                          (anchor 'center))
  (make-object texture pos (render-texture anchor)))

(define <scene> (make-record-type '<scene> '(layers)))
(define %make-scene (record-constructor <scene>))
(define scene-layers (record-accessor <scene> 'layers))
(define set-scene-layers! (record-modifier <scene> 'layers))

;; Allocate a scene consisting of @var{layers}, a list of symbols.
(define (make-scene layers)
  (%make-scene (map list layers)))

;; Return the objects in @var{scene} associated with @var{layer}.
(define (scene-layer scene layer)
  (assq-ref (scene-layers scene) layer))

;; Set @var{layer} in @var{scene} to @var{objs}.
(define (set-scene-layer! scene layer objs)
  (let ((real-layer (assq layer (scene-layers scene))))
    (set-cdr! real-layer objs)))

;; Add @var{object} to @var{layer} in @var{scene}.  If @var{append?} is
;; @code{#t} (the default), append it at the end, so that it will be rendered
;; on top of any other object currently in @var{layer}.
(define* (scene-add-object! scene layer object #:optional (append? #t))
  (set-scene-layer! scene layer
                    (if append?
                        (append (scene-layer scene layer)
                                (list object))
                        (cons object (scene-layer scene layer)))))

;; Append @var{objects} to the end of @var{layer} in @var{scene}.
(define (scene-add-objects! scene layer objects)
  (set-scene-layer! scene layer
                    (append (scene-layer scene layer) objects)))

;; Like @code{scene-add-object!}, but creating a new object from @var{real}
;; at @var{pos}, that will be rendered using @var{render}.  Return the object
;; that is thus formed.
(define* (scene-add! scene layer real render
                     #:key (pos #s32(0 0))
                     (append? #t))
  (let ((object (make-object real pos render)))
    (scene-add-object! scene layer object append?)
    object))

;; Clear @var{layers} in @var{scene}.
(define (scene-clear! scene . layers)
  (for-each
   (lambda (layer)
     (set-scene-layer! scene layer '()))
   layers))

;; Remove @var{object} from @var{layer} in @var{scene}.
(define (scene-remove-object! scene layer object)
  (set-scene-layer! scene layer
                    (delete (scene-layer scene layer) object eq?)))

(define (draw-scene scene)
  (for-each
   (lambda (layer)
     (for-each render-object (cdr layer)))
   (scene-layers scene)))
