;; Copyright (C) 2020, Leo Prikler <leo.prikler@student.tugraz.at>
;;
;; This library is free software; you can redistribute it and/or
;; modify it under the terms of the GNU Lesser General Public
;; License as published by the Free Software Foundation; either
;; version 3 of the License, or (at your option) any later version.
;;
;; This library is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; Lesser General Public License for more details.
;;
;; You should have received a copy of the GNU Lesser General Public
;; License along with this library; if not, write to the Free Software
;; Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA

(define-module (tsukundere preferences)
  #:use-module (ice-9 hash-table)
  #:use-module (ice-9 match)
  #:use-module (ice-9 optargs)
  #:use-module (ice-9 pretty-print)
  #:use-module (tsukundere utils)
  #:export (make-preference
            load-preferences
            save-preferences
            add-preference-hook!))

(define *preferences* (make-hash-table))
(define preference-accessor (make-object-property))
(define preference-hook (make-object-property))

;; Create a new preference named @var{name} with default value @var{default}.
;; Return a procedure with setter, that can be used to access and store the
;; preferences' value.
;; When set through the setter, use @var{validate?} to check whether the value
;; passed in is valid for the preference.  Also set @var{documentation} as the
;; procedure's documentation if given.
(define* (make-preference name default
                          #:key (validate? (const #t))
                          documentation)
  (unless (symbol? name)
    (error "not a valid preference key: ~s" name))
  (let* ((handle (hashq-create-handle! *preferences* name
                                       (make-variable default)))
         (var (cdr handle)))
    (unless (preference-hook var)
      (set! (preference-hook var) (make-hook 1)))

    (let ((proc
           (make-procedure-with-setter
            (lambda ()
              (variable-ref var))
            (lambda (value)
              (when (validate? value)
                (variable-set! var value)
                (run-hook (preference-hook var) value))))))
      (when documentation
        (set-procedure-property! proc 'documentation documentation))
      (set! (preference-accessor var) proc)
      (set! (preference-hook proc) (preference-hook var))
      proc)))

(define (%load-preferences file)
  (call-with-input-file file
    (lambda (port)
      (for-each
       (lambda (kv)
         (let ((key (car kv))
               (value (cdr kv)))
           (let ((var (hashq-ref *preferences* key)))
             (when var
               (set! ((preference-accessor var)) value)))))
       (read port)))))

(define* (load-preferences #:optional game)
  (let ((file (tsukundere-config-file
               (if game
                   (string-append game ".preferences")
                   "preferences"))))
    (when (file-exists? file)
      (%load-preferences file))))

(define (%save-preferences file)
  (call-with-output-file file
    (lambda (port)
      (pretty-print
       (hash-map->list
        (lambda (key value)
          (cons key (variable-ref value)))
        *preferences*)
       port))))

(define (add-preference-hook! preference callback)
  (add-hook! (preference-hook preference) callback))

(define* (save-preferences #:optional game (when? #t))
  (let ((file (tsukundere-config-file
               (if game
                   (string-append game ".preferences")
                   "preferences"))))
    (mkdir-p (tsukundere-config-dir))
    (match (list when? (file-exists? file))
      ((#t _) (%save-preferences file))
      (('if-exists? #t) (%save-preferences file))
      (('unless-exists? #f) (%save-preferences file))
      ((_ _) *unspecified*))))
