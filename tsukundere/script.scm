;; Copyright (C) 2020, Leo Prikler <leo.prikler@student.tugraz.at>
;;
;; This library is free software; you can redistribute it and/or
;; modify it under the terms of the GNU Lesser General Public
;; License as published by the Free Software Foundation; either
;; version 3 of the License, or (at your option) any later version.
;;
;; This library is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; Lesser General Public License for more details.
;;
;; You should have received a copy of the GNU Lesser General Public
;; License along with this library; if not, write to the Free Software
;; Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA

(define-module (tsukundere script)
  #:use-module (tsukundere agenda)
  #:use-module (tsukundere game internals)
  #:use-module (srfi srfi-9)
  #:export (make-script
            fresh-script start-script script-status set-script-status! resume
            script-line script-clock script-time
            speed-through-clock! forward-line!
            script)
  #:replace (pause sleep))

(define script-prompt (make-prompt-tag 'tsukundere:script))

(define-record-type <script>
  (%make-script thunk line clock status cont)
  script?
  (thunk  script-thunk)
  (line   %script-line)
  (clock  script-clock)
  (status script-status set-script-status!)
  (cont   script-resume set-script-resume!))

;; Allocate a new script for @var{thunk}.
(define (make-script thunk)
  (%make-script thunk (make-agenda) (make-agenda) 'not-started
                (const *unspecified*)))

;; Return the current `line' of @var{script}, i.e. how often
;; @code{forward-line!} was called.
(define (script-line script)
  (agenda-time (%script-line script)))

;; Return the time spent in @var{script} in milliseconds.
(define (script-time script)
  (agenda-time (script-clock script)))

;; Fast-forward through all tasks, that are waiting for more time to be spent
;; in @var{script}.
(define (speed-through-clock! script)
  "Run all tasks scheduled in the clock, if any.  Return #t if tasks were run, else #f."
  (let ((clock (script-clock script)))
    (if (agenda-filled? clock)
        (begin
          (finish-agenda! clock)
          #t)
        #f)))

;; Forward @var{script} by one `line'.
(define (forward-line! script)
  (update-agenda! (%script-line script) 1))

;; Return a copy of @var{script}, that is not yet started.
(define (fresh-script script)
  (if (eq? (script-status script) 'not-started)
      script
      (make-script (script-thunk script))))

(define* (start-script script #:key (status 'running))
  (unless (eq? (script-status script) 'not-started)
    (error "attempted to start already started script ~s" script))
  (let ((script (fresh-script script))
        (thunk (script-thunk script)))
    (define (handler cont callback)
      (set-script-resume!
       script
       (lambda ()
         (unless (eq? (script-status script) 'cancelled)
           (call-with-prompt script-prompt cont handler))))
      (callback script))
    (set-script-status! script status)
    (call-with-prompt script-prompt
      (lambda ()
        (with-fluid* *script* script thunk)
        (set-script-status! script 'finished))
      handler)
    script))

;; Resume @var{script}.
(define (resume script)
  ((script-resume script)))

(define (%default-pause-handler script)
  (schedule-after! (%script-line script) 1 (script-resume script)))

;; Pause the current script until @var{handler} resumes it.
;; By default handler waits for the player to forward it using @code{step!}.
(define* (pause #:optional (handler %default-pause-handler))
  "Pause the current script until HANDLER calls it again.
By default, that is until step! or skip! is called."
  (abort-to-prompt script-prompt handler))

;; Pause the current script for @var{duration} milliseconds.
(define (sleep duration)
  "Wait DURATION before resuming the current script."
  (pause (lambda (script)
           (schedule-after! (script-clock script) duration
                            (script-resume script)))))

(define-syntax step
  (syntax-rules ()
    ((step exp)
     (begin (pause) exp))
    ((step exp0 exp1 ...)
     (begin exp0 (step exp1) ...))))

(define-syntax-rule (script exp0 exp1 ...)
  (begin
    (step exp0 exp1 ...)))
