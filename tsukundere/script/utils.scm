;; Copyright (C) 2020, Leo Prikler <leo.prikler@student.tugraz.at>
;;
;; This library is free software; you can redistribute it and/or
;; modify it under the terms of the GNU Lesser General Public
;; License as published by the Free Software Foundation; either
;; version 3 of the License, or (at your option) any later version.
;;
;; This library is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; Lesser General Public License for more details.
;;
;; You should have received a copy of the GNU Lesser General Public
;; License along with this library; if not, write to the Free Software
;; Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA

(define-module (tsukundere script utils)
  #:use-module (tsukundere agenda)
  #:use-module (tsukundere history)
  #:use-module (tsukundere math)
  #:use-module (tsukundere game)
  #:use-module (tsukundere game internals)
  #:use-module (tsukundere scene)
  #:use-module (tsukundere sound)
  #:use-module (tsukundere script)
  #:use-module (tsukundere textures)
  #:use-module (tsukundere text)
  #:use-module (sdl2 surface)
  #:use-module (sdl2 video)
  #:use-module (ice-9 arrays)
  #:use-module (ice-9 hash-table)
  #:use-module (ice-9 match)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-26)
  #:export (tween
            add! add-object! clear!
            current-window-width current-window-height
            current-window-center background
            say make-speaker call
            make-person-type person->speaker
            roll-credits
            input)
  #:re-export ((play-sound . sound)
               (play-music . music)
               (set-object-pos! . move!)
               script
               history
               (call-with-game-loop/paused . call))
  #:re-export-and-replace (pause sleep))

;; Call @var{proc} every @var{step} milliseconds for a total of @var{duration},
;; with values interpolated between @var{start} and @var{end} according to
;; @var{ease} and @var{interpolation}.
(define* (tween duration start end proc #:key
                (step 1)
                (ease smoothstep)
                (interpolate lerp))
  (let* ((script (fluid-ref *script*))
         (t0 (script-time script)))
    (let loop ((t t0)
               (t* (+ t0 duration)))
      (if (>= t t*)
          (proc end)
          (let ((alpha (ease (/ (- t t0) (- t* t0)))))
            (proc (interpolate start end alpha))
            (sleep step)
            (loop (script-time script) t*))))))

(define add! (cut scene-add! (fluid-ref *scene*) <> <> <> <...>))
(define add-object! (cut scene-add-object! (fluid-ref *scene*) <> <> <...>))
(define clear! (cut scene-clear! (fluid-ref *scene*) <>))

(define current-window-width
  (compose car window-size current-window))

(define current-window-height
  (compose cadr window-size current-window))

(define (current-window-center)
  (list->s32vector
   (map (lambda (x) (floor/ x 2))
        (window-size (current-window)))))

;; Set @var{surface-or-texture} as the current background, optionally
;; positioning it at @var{pos}.  It will always be anchored in the center.
(define* (background surface-or-texture #:key
                     (pos (current-window-center)))
  "Set the background for SCENE to TEXTURE.
Optionally move it to POS, scale it according to SCALE and rotate it according
to ROTATION."
  (clear! 'background)
  (add-object! 'background
               (if (surface? surface-or-texture)
                   (surface->object surface-or-texture #:pos pos)
                   (texture->object surface-or-texture #:pos pos))))

;; Let @var{speaker} say @var{text}.  If @var{append?} is true, use
;; @code{text-add!} to add the text to the end of what is currently displayed
;; rather than resetting it.
(define* (say speaker text #:optional append?)
  (match (scene-layer (fluid-ref *scene*) 'text)
    ((%speaker %text)
     (reset-text! %speaker speaker #t)
     (if append?
         (text-add! %text text)
         (reset-text! %text text)))))

;; Make a procedure, that calls @code{say} with the rest of its arguments
;; binding @var{speaker} to @var{name}.
(define (make-speaker name)
  (cute say name <> <...>))

(define <person>
  (make-record-type '<person> '((immutable name) sprite) #:parent <object>
                    #:extensible? #t))
(define person-name               (record-accessor <person> 'name))
(define person-active-sprite      (record-accessor <person> 'sprite))
(define set-person-active-sprite! (record-modifier <person> 'sprite))
(define (%person-render anchor sprites)
  (let ((real-render (render-texture anchor)))
    (lambda (person)
      (real-render
       (hashq-ref sprites (person-active-sprite person))
       (object-pos person)))))

;; Convert @var{person} into a speaker.
(define (person->speaker person)
  (make-speaker (person-name person)))

;; Make a new person type @var{type} with @var{fields}, whose sprites can be
;; loaded with @var{pattern}.
;; @*
;; In addition to the field specifications allowed in @code{make-record-type},
;; a field can be a sprite field indicated by either
;; @code{(mutable sprite field)} or @code{(immutable sprite field)}
;; (the positions of mutable/immutable and sprite can be inverted).
;; The values of sprite fields shall always be symbols and are joined
;; in the order they were specified in to form lookup keys to the table
;; returned by @code{load-images}.  Likewise the names of the sprite fields
;; are passed as keys to @code{load-images} so as to form that table and
;; should appear in the same order as they are in pattern.
;; At least one sprite field is required.
;; @*
;; Return a person-loader defined as follows
;; @deffn procedure load-person name dir [pos] [anchor] . rest
;; Load a person with name @var{NAME} from @var{dir}, optionally initializing
;; its position at @var{pos} and rendering it at anchor @var{anchor}, using
;; @var{rest} to initialize the rest of its fields.
;; @end deffn
;; as well as a list of fields formatted like (@var{field} @var{accessor}
;; @var{modifier}), where @var{field} is the name of the field, that was
;; passed in, @var{accessor} is a procedure, by which it can be accessed
;; and @var{modifier} is a procedure, by which it can be modified.
(define (make-person-type type pattern fields)
  (define %has-sprite
    (map
     (lambda (f) (and pair? f) (member 'sprite f))
     fields))
  (unless (any identity %has-sprite)
    (error "No sprite field in ~s" fields))
  (define %fields
    (map
     (lambda (field)
       (if (pair? field) (delete 'sprite field) field))
     fields))
  (define person-type (make-record-type type %fields #:parent <person>))
  (define %field-names
    (map
     (lambda (field)
       (if (pair? field) (last field) field))
     fields))
  (define sprite-fields
    (filter-map
     (lambda (take? name)
       (and take name))
     %has-sprite %field-names))
  (define accessors (map (cute record-accessor person-type <>) %field-names))
  (define sprite-accessors
    (filter-map
     (lambda (take? acc)
       (and take? acc))
     %has-sprite accessors))
  (define (refresh-active-sprite! person)
    (set-person-active-sprite!
     person
     (false-if-exception
      (string->symbol
       (string-join (map (compose
                          symbol->string
                          (cute <> person))
                         sprite-accessors)
                    "/"))))
    person)
  (define modifiers
    (map
     (lambda (wrap? field)
       (let ((mod (record-modifier person-type field)))
         (if wrap?
             (lambda (person value)
               (mod person value)
               (refresh-active-sprite! person)
               value)
             mod)))
     %has-sprite %field-names))

  (define* (load-person name dir #:optional (pos #s32(0 0)) (anchor 'center)
                        #:rest field-init)
    (refresh-active-sprite!
     (apply
      (record-constructor person-type)
      pos
      (%person-render
       anchor
       (alist->hashq-table (load-images dir pattern sprite-fields)))
      name
      *unspecified*
      (append field-init
              (make-list (- (length %fields) (length field-init))
                         *unspecified*)))))

  (values load-person (map list %field-names accessors modifiers)))

;; Roll the credits in @var{scene}, which span a total vertical size of
;; @var{length} pixels, for @var{duration} milliseconds.
;; While the credits are rolling, vertically scroll them from the bottom of the
;; current window to the top before resetting their position.
(define (roll-credits scene length duration)
  (let* ((credits-layer (scene-layer scene 'credits))
         (pos0 (map object-pos credits-layer)))
    (call-with-game-loop/paused
     (lambda ()
       (for-each set-object-pos! credits-layer (map array-copy pos0))
       (tween duration (current-window-height) (- length)
              (lambda (y)
                (for-each
                 (lambda (obj p)
                   (array-map! (object-pos obj)
                               + p (s32vector 0 (round y))))
                 credits-layer pos0))
              #:ease identity)
       (for-each set-object-pos! credits-layer pos0))
     #:scene scene
     #:key-press (const *unspecified*)
     #:key-release (const *unspecified*)
     #:mouse-press (const *unspecified*)
     #:mouse-release (const *unspecified*))))

(define* (%input speaker prompt)
  (let ((input (make-fluid ""))
        (%running (make-fluid #t)))
    (call-with-game-loop/paused
     (lambda ()
       (while (fluid-ref %running)
         (match (scene-layer (fluid-ref *scene*) 'text)
           ((%speaker %text)
            (reset-text! %speaker speaker #t)
            (reset-text! %text
                         (string-join (list prompt (fluid-ref input)) "\n")
                         #t)))
         (sleep 1)))
     #:key-press
     (lambda (key code mod repeat?)
       (case key
         ((return) (fluid-set! %running #f))
         ((backspace)
          (when (fluid-ref %running)
            (let ((i (fluid-ref input)))
              (fluid-set! input
                          (substring i 0
                                     (max 0 (1- (string-length i))))))))))
     #:key-release (const *unspecified*)
     #:mouse-press (const *unspecified*)
     #:mouse-release (const *unspecified*)
     #:text-input
     (lambda (txt)
       (when (fluid-ref %running)
         (fluid-set! input
                     (string-append (fluid-ref input) txt)))))
    (fluid-ref input)))

(define input
  (case-lambda
    ((prompt) (%input "" prompt))
    ((speaker prompt) (%input speaker prompt))))
