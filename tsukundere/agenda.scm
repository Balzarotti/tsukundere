;; Copyright (C) 2020, Leo Prikler <leo.prikler@student.tugraz.at>
;;
;; This library is free software; you can redistribute it and/or
;; modify it under the terms of the GNU Lesser General Public
;; License as published by the Free Software Foundation; either
;; version 3 of the License, or (at your option) any later version.
;;
;; This library is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; Lesser General Public License for more details.
;;
;; You should have received a copy of the GNU Lesser General Public
;; License along with this library; if not, write to the Free Software
;; Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA

(define-module (tsukundere agenda)
  #:use-module (srfi srfi-2)
  #:use-module (srfi srfi-9)
  #:export (make-agenda
            agenda-time

            update-agenda!
            reset-agenda!
            finish-agenda!
            agenda-filled?

            schedule-at!
            schedule-after!))

(define-record-type <agenda>
  (%make-agenda time tasks)
  agenda?
  (time agenda-time set-agenda-time!)
  (tasks agenda-tasks set-agenda-tasks!))

(define (make-agenda)
  "Allocate a new agenda."
  (%make-agenda 0 '()))

(define (pop-task? agenda)
  (let ((tasks (agenda-tasks agenda)))
   (and (not (null? tasks))
        (<= (caar tasks) (agenda-time agenda))
        (let ((thunk (cdar tasks)))
          (set-agenda-tasks! agenda (cdr tasks))
          thunk))))

(define (update-agenda! agenda dt)
  "Update AGENDA by DT, running any tasks, that might have been scheduled
in the meantime."
  (set-agenda-time! agenda (+ (agenda-time agenda) dt))
  (let loop ((thunk (pop-task? agenda)))
    (when thunk
      (thunk)
      (loop (pop-task? agenda)))))

(define (reset-agenda! agenda)
  "Clear AGENDA and reset the internal time."
  (set-agenda-tasks! agenda '())
  (set-agenda-time! agenda 0))

(define (finish-agenda! agenda)
  "Run all agenda tasks until completion. This may take forever."
  (while (not (null? (agenda-tasks agenda)))
    (update-agenda! agenda 1)))

(define (agenda-filled? agenda)
  "Return #t if there is a task in AGENDA, that is not yet run."
  (not (null? (agenda-tasks agenda))))

(define (%insert-task tasks task)
  (let loop ((previous '())
             (next tasks))
    (cond
     ((null? next)
      (reverse! previous (list task)))
     ((<= (caar tasks) (car task))
      (loop (cons (car tasks) previous) (cdr tasks)))
     (else
      (reverse! previous (cons task next))))))

(define (schedule-at! agenda time thunk)
  "Schedule THUNK to be executed at TIME in AGENDA."
  (when (< time (agenda-time agenda))
    (error "cannot schedule task in the past: ~a < ~a"
           time (agenda-time agenda)))
  (set-agenda-tasks! agenda
                     (%insert-task (agenda-tasks agenda) (cons time thunk))))

(define (schedule-after! agenda time thunk)
  "Schedule THUNK to be executed after TIME steps have elapsed in AGENDA."
  (schedule-at! agenda (+ (agenda-time agenda) time) thunk))
