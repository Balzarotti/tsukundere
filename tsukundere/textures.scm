;; Copyright (C) 2020, Leo Prikler <leo.prikler@student.tugraz.at>
;;
;; This library is free software; you can redistribute it and/or
;; modify it under the terms of the GNU Lesser General Public
;; License as published by the Free Software Foundation; either
;; version 3 of the License, or (at your option) any later version.
;;
;; This library is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; Lesser General Public License for more details.
;;
;; You should have received a copy of the GNU Lesser General Public
;; License along with this library; if not, write to the Free Software
;; Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA

(define-module (tsukundere textures)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-2)
  #:use-module (srfi srfi-26)
  #:use-module ((sdl2 image) #:prefix sdl2:)
  #:use-module (sdl2 render)
  #:use-module (sdl2 surface)
  #:use-module ((tsukundere game internals) #:select (current-renderer))
  #:use-module (tsukundere utils)
  #:export (load-image
            load-images
            surface->texture*
            render-texture

            texture-width texture-height))

(define texture-width (make-object-property))
(define texture-height (make-object-property))

;; Convert @var{surface} to a texture like @code{surface->texture}, but also
;; remember its width and height.
(define (surface->texture* surface . args)
  (let ((texture (apply surface->texture (current-renderer) surface args)))
    (set! (texture-width texture) (surface-width surface))
    (set! (texture-height texture) (surface-height surface))
    texture))

;; Load an image from @var{file} and convert it into a texture.
(define (load-image file)
  (surface->texture* (sdl2:load-image file)))

;; Load images from files in @var{directory} matching the PEG pattern
;; @var{pattern} and associate them with @var{keys}.
(define (load-images directory pattern keys)
  (load-assets load-image directory pattern keys))

(define (%render-texture texture x y w h)
  (render-copy (current-renderer)
               texture
               #:srcrect (list 0 0 w h)
               #:dstrect (list x y w h)))

(define (render-texture/top-left texture pos)
  (let* ((w (texture-width texture))
         (h (texture-height texture))
         (x (s32vector-ref pos 0))
         (y (s32vector-ref pos 1)))
    (%render-texture texture x y w h)))

(define (render-texture/center texture pos)
  (let* ((w (texture-width texture))
         (h (texture-height texture))
         (x (- (s32vector-ref pos 0) (floor/ w 2)))
         (y (- (s32vector-ref pos 1) (floor/ h 2))))
    (%render-texture texture x y w h)))

(define (render-texture/bottom-left texture pos)
  (let* ((w (texture-width texture))
         (h (texture-height texture))
         (x (s32vector-ref pos 0))
         (y (- (s32vector-ref pos 1) h)))
    (%render-texture texture x y w h)))

;; Produce a texture renderer anchored in @var{anchor}, a symbol.
;; The returned renderer takes two arguments, @var{texture} and @var{pos} and
;; renders @var{texture} so that its anchor is at @var{pos} with @var{pos}
;; being measured from the top left of the screen.
;; Valid values for @var{anchors are}
;; @itemize
;; @item @code{top-left}: the top left pixel.
;; @item @code{center}: a pixel, that is roughly in the center of the texture.
;; @item @code{bottom-left}: the bottom left pixel.
;; @end itemize
(define (render-texture anchor)
  (case anchor
    ((top-left) render-texture/top-left)
    ((center) render-texture/center)
    ((bottom-left) render-texture/bottom-left)
    (else (error "anchor position not supported"))))
