;; Copyright (C) 2020, Leo Prikler <leo.prikler@student.tugraz.at>
;;
;; This library is free software; you can redistribute it and/or
;; modify it under the terms of the GNU Lesser General Public
;; License as published by the Free Software Foundation; either
;; version 3 of the License, or (at your option) any later version.
;;
;; This library is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; Lesser General Public License for more details.
;;
;; You should have received a copy of the GNU Lesser General Public
;; License along with this library; if not, write to the Free Software
;; Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA

(define-module (tsukundere sound)
  #:use-module (ice-9 optargs)
  #:use-module (tsukundere history)
  #:use-module (tsukundere utils)
  #:use-module ((sdl2 mixer) #:prefix mixer:)
  #:re-export ((mixer:load-chunk . load-sound)
               (mixer:load-music . load-music)
               (mixer:stop-music! . stop-music!))
  #:export (load-sounds load-music* play-sound play-music))

;; @deffnx {(tsukundere script utils) alias} sound
;; Play sound file @var{sound} in @var{channel} looping it @var{loops} times.
;; If @var{volume} is given, first set the volume of the sound file to it.
(define* (play-sound sound #:key (loops 0) channel volume)
  (when volume
    (mixer:set-chunk-volume! sound volume))
  (mixer:play-chunk! sound #:loops loops #:channel channel))

;; @deffnx {(tsukundere script utils) alias} music
;; Play music file @var{music} in @var{channel} looping it @var{loops} times.
;; If @var{volume} is given, first set the music volume to it.
;; If @var{music} is @code{#f}, stop playing instead.
(define* (play-music music #:key (loops 0) volume)
  (when volume
    (mixer:set-music-volume! volume))
  (if music
      (mixer:play-music! music loops)
      (mixer:stop-music!)))

;; Load sounds from files in @var{directory} matching the PEG pattern
;; @var{pattern} and associate them with @var{keys}.
(define (load-sounds directory pattern keys)
  "Load sounds conforming to PEG pattern PATTERN from DIRECTORY, associating
them by KEYS."
  (load-assets mixer:load-chunk directory pattern keys))

;; Load music from files in @var{directory} matching the PEG pattern
;; @var{pattern} and associate them with @var{keys}.
(define (load-music* directory pattern keys)
  "Load music conforming to PEG pattern PATTERN from DIRECTORY, associating
them by KEYS."
  (load-assets mixer:load-music directory pattern keys))
