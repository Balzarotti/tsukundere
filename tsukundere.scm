;; Copyright (C) 2020, Leo Prikler <leo.prikler@student.tugraz.at>
;;
;; This library is free software; you can redistribute it and/or
;; modify it under the terms of the GNU Lesser General Public
;; License as published by the Free Software Foundation; either
;; version 3 of the License, or (at your option) any later version.
;;
;; This library is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; Lesser General Public License for more details.
;;
;; You should have received a copy of the GNU Lesser General Public
;; License along with this library; if not, write to the Free Software
;; Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA

(define-module (tsukundere)
  #:use-module (tsukundere game)
  #:use-module (tsukundere history)
  #:use-module (tsukundere scene)
  #:use-module (tsukundere script)
  #:use-module (tsukundere script utils)
  #:use-module (tsukundere sound)
  #:use-module (tsukundere text)
  #:use-module (tsukundere textures)

  #:re-export (run-game
               call-with-game-loop
               call-with-game-loop/paused
               init-script!
               init-text!
               current-window
               current-renderer

               add-load-hook! history

               script tween
               add! add-object! clear! move!
               current-window-center background
               say make-speaker call input
               make-person-type person->speaker
               roll-credits

               load-sound load-sounds
               load-music load-music*
               play-sound sound
               play-music music

               load-image load-images
               render-texture

               make-scene make-object make-text
               surface->object
               texture->object

               scene-add!
               scene-add-object!
               scene-add-objects!
               scene-clear!
               scene-remove-object!)
  #:re-export-and-replace (pause sleep))
