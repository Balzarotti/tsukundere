# -*- mode: org; -*-
#+TITLE: Tsukundere News
#+STARTUP: content hidestars

Copyright © Leo Prikler <leo.prikler@student.tugraz.at>

* Changes since 0.2.1
** Preferences

Tsukundere now loads and stores global and per-game preferences in
~$XDG_CONFIG_DIR/tsukundere~.

** Added scaling and fullscreen support

Tsukundere windows should now be scalable on your desktop and a fullscreen
mode can be enabled (and disabled) by pressing F11.  The default behaviour
can be changed via the ~fullscreen?~ preference.

** Fixed bug in input

The ~input~ script utility no longer crashes when backspacing on an already
empty string.

* Changes in 0.2.1 (since 0.2.0)
** Fixed bug in state saving

The directory in which save files should be put will now be created, as it
should.  Saving still silently fails in case it couldn't.

** Added documentation

Tsukundere now has (mostly snarfed) documentation.

* Changes in 0.2.0 (since 0.1.1)
** Changes in initialization

- ‘run-game!’ no longer supports overriding the default event handler.
  Instead, internal procedures will be used to initialize it.
- The ‘load’ paramter has been renamed to ‘init!’.
- The frequency and chunk size used by the mixer can now be configured
  using ‘mixer-frequency’ and ‘mixer-chunk-size’ respectively.

** Extensible objects and person types

It is now possible to extend the <object> type, that is used internally for
rendering.  This feature is meant primarily for internal use, so the <object>
type is not actually exposed from the (tsukundere) module.

This feature also enables custom person types, where a person holds multiple
textures of which only one is active.  Using the modifiers returned by
‘make-person-type’ this active texture is also updated.

** Improved event handling and text input support

Tsukundere now allows for more events to be caught by call-with-game-loop and
its variants, including mouse move events and text input events.
The latter have been used to implement the ‘input’ procedure, which captures
text input from the user in a string.

** Per-script timing

Scripts now store the agendas they use for scheduling as well as their
continuations.  It is now possible to call arbitrary thunks in a new context,
but doing so endangers save files if saving is not disabled.

* Changes in 0.1.1 (since 0.1.0)
** Added Continuous Integration

Tsukundere will now waste precious computation resources, I mean, uh… perform
very necessary procedures to ensure the Best Quality™ of the package.

** The default window title is now "Tsukundere"

As it should be.

** Ported to GNU Build System

Instead of "needing" Guix' guile-build-system, you can now use "./configure"
followed by "make".
