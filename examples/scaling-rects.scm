(use-modules (tsukundere)
             (sdl2 rect)
             (sdl2 render))

(define (load)
  (init-script!
   (lambda () (pause))
   (make-scene '(dot)))
  (add! 'dot
        (list (make-rect 0 0 100 100)
              (make-rect 100 100 100 100)
              (make-rect 200 200 100 100)
              (make-rect 300 300 100 100))
        (lambda (rects pos)
          (set-render-draw-color (current-renderer) 255 255 255 255)
          (render-fill-rects (current-renderer) rects)
          (set-render-draw-color (current-renderer) 0 0 0 255))))

(run-game
 #:game-name ".tsukundere-example-scaling-rects"
 #:window-width 400
 #:window-height 400
 #:init! load)
